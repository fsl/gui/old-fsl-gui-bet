import { useState, useEffect, useRef, memo } from 'react'
import Box from '@mui/material/Box'
import LoadingButton from '@mui/lab/LoadingButton';
import { Button, Card, CardContent, Tooltip, Slider, Input, Collapse, Snackbar, FormControlLabel, Checkbox, Switch } from '@mui/material'
import { Container, CssBaseline, TextField, Typography, Grid, Divider } from '@mui/material'
import { Niivue, NVImage } from '@niivue/niivue'
import { io } from "socket.io-client";
import "./App.css"

// get web socket connection info from url before rendering anything
let urlParams = new URLSearchParams(window.location.search)
let host = urlParams.get('host') || import.meta.env.VITE_FSLGUI_HOST
let socketServerPort = urlParams.get('socketServerPort') || import.meta.env.VITE_FSLGUI_WS_PORT
let fileServerPort = urlParams.get('fileServerPort') || import.meta.env.VITE_FSLGUI_FS_PORT
let useShortDisplay = urlParams.get('shortNames') // can be set from global FSL settings on user's machine

// different socket clients for different widgets
//const inFileSocket = io(`ws://${host}:${socketServerPort}`)
//const runSocket = io(`ws://${host}:${socketServerPort}`)
//const fsleyesSocket = io(`ws://${host}:${socketServerPort}`)


const nv = new Niivue({clipPlaneColor:[0, 0, 0, 0]})

async function addNiiVueImage(url, color='gray') {
	console.log(url)
	let img = await NVImage.loadFromUrl(url,'',color,1)
	console.log(img)
	nv.addVolume(img)
}

const NiiVue = ({url}) => {
	// use url string (primative) since object equality will not work with effects
	const canvas = useRef()
	useEffect(() => {
		nv.attachToCanvas(canvas.current)
		nv.loadVolumes([{url:url}])
	}, [])

	return (
		<Grid container item xs={12} m={2} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<canvas ref={canvas} height={200} width={640}></canvas>
	</Grid>
	)
}

function InputField({text, updateBetOptsValue}) {
	const [inFileSocket, setInFileSocket] = useState({})
	useEffect(() => {
		const sock = io(`ws://${host}:${socketServerPort}`)
		sock.on('files', (data) => {
			updateBetOptsValue('input', data[0] ? data[0] : '')
			if (data[0] !== '') {
				addNiiVueImage(`http://${host}:${fileServerPort}/file/?filename=${data[0]}`)
			}
		})
		setInFileSocket(sock)
	}, [])
	
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='input file' 
					onInput={(e) => {updateBetOptsValue('input', e.target.value)}}
					value={text}>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{inFileSocket.emit('files', {filters:[{name: 'NIFTI', extensions:['nii','nii.gz']}]})}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function Title({text}) {
	return (
		<Typography variant='h5' sx={{margin: '20px'}}>
			{text}
		</Typography>
	)
}

function OutputField({text, updateBetOptsValue}) {
	const [outFileSocket, setOutFileSocket] = useState({})
	useEffect(() => {
		const sock = io(`ws://${host}:${socketServerPort}`)
		sock.on('save', (data) => {
			updateBetOptsValue('output', data['filePath'] ? data['filePath'] : '')
		})
		setOutFileSocket(sock)
	}, [])
	return (
		<Grid container item xs={12} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<TextField 
					fullWidth 
					size="small" 
					label='output file'
					onInput={(e) => {updateBetOptsValue('output', e.target.value)}}
					value={text}
				>
				</TextField>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'flex-end'}}>
				<Button 
					variant='contained' 
					style={{margin:0, marginLeft: 12}}
					onClick={()=>{outFileSocket.emit('save', {
						filters:[{name: 'NIFTI', extensions:['nii','nii.gz']}],
						defaultPath: text || ''
					})}}
				>
					Choose
				</Button>
			</Grid>
		</Grid>
	)
}

function GvalueField({g, updateBetOptsValue}) {
	return (
		<Grid container item xs={12} sx={{marginLeft:'2px'}} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<Typography>g value</Typography>
				<Slider
					onChange={(e) => {updateBetOptsValue('-g', e.target.value)}}
					value={Number(g)  ?? 0}
					min={-1.0}
					max={1.0}
					step={-0.01}
				>
				</Slider>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'center'}}>
				<Input
					size='small'
					style={{margin:0, marginLeft:12}}
					value={Number(g)  ?? 0}
					inputProps={{
						step: 0.1,
						min: -1,
						max: 1,
						type: 'number'
					}}
					onInput={(e) => {updateBetOptsValue('-g', e.target.value)}}
				/>
			</Grid>
		</Grid>
	)
}


function FvalueField({f, updateBetOptsValue}) {
	return (
		<Grid container item xs={12} sx={{marginLeft:'2px'}} alignItems='center' spacing={0}>
			<Grid item xs={9}>
				<Typography>f value</Typography>
				<Slider
					onChange={(e) => {updateBetOptsValue('-f', e.target.value)}}
					value={Number(f)  ?? 0.5}
					min={0.0}
					max={1.0}
					step={0.01}
				>
				</Slider>
			</Grid>
			<Grid item xs={3} sx={{justifyContent: 'center'}}>
				<Input
					size='small'
					style={{margin:0, marginLeft:12}}
					value={Number(f)  ?? 0.5}
					inputProps={{
						step: 0.1,
						min: 0,
						max: 1,
						type: 'number'
					}}
					onInput={(e) => {updateBetOptsValue('-f', e.target.value)}}
				/>
			</Grid>
		</Grid>
	)
}

function ActionButtons({handleMoreOptions, commandString, isRunning, setIsRunning, runSocket}) {
	return (
		<Grid container item xs={12} alignItems='center' justifyContent='center' spacing={0} direction='row'>
			<LoadingButton
				onClick={()=>{runSocket.emit('run', {'run': commandString}); setIsRunning(true)}}
        loading={isRunning}
        loadingPosition="end"
        variant="contained"
				style={{margin:0}}
      >
        Run
      </LoadingButton>
			<Button style={{margin:0}} onClick={handleMoreOptions}>more options</Button>	
		</Grid>
	)
}

function BetOptionCheckBox({optionValue, updateBetOptsValue, label}) {
	return (
		<FormControlLabel 
			control={<Checkbox checked={optionValue} onChange={updateBetOptsValue}/>}
			label={label}
		/>
	)
}

function OptionsContainer({moreOptions, options, updateBetOptsValue}) {
	return (
		<Collapse in={moreOptions} timeout="auto" unmountOnExit>
			<Grid container item xs={12} m={2} spacing={0} direction='column'>
				<GvalueField g={options['-g']} updateBetOptsValue={updateBetOptsValue} />
				<BetOptionCheckBox
					optionValue={options['-o']}
					label={'create brain outline image'}
					updateBetOptsValue={() => {updateBetOptsValue('-o', !options['-o'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-m']}
					label={'output binary brain mask'}
					updateBetOptsValue={() => {updateBetOptsValue('-m', !options['-m'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-s']}
					label={'output approximate skull image'}
					updateBetOptsValue={() => {updateBetOptsValue('-s', !options['-s'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-n']}
					label={'do not output final brain image'}
					updateBetOptsValue={() => {updateBetOptsValue('-n', !options['-n'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-t']}
					label={'apply thresholding to brain and mask'}
					updateBetOptsValue={() => {updateBetOptsValue('-t', !options['-t'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-e']}
					label={'save brain surface mesh'}
					updateBetOptsValue={() => {updateBetOptsValue('-e', !options['-e'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-v']}
					label={'switch on diagnostic messages'}
					updateBetOptsValue={() => {updateBetOptsValue('-v', !options['-v'])}}
				/>
				<BetOptionCheckBox
					optionValue={options['-d']}
					label={'debug: do not delete intermediate images'}
					updateBetOptsValue={() => {updateBetOptsValue('-d', !options['-d'])}}
				/>
			</Grid>
		</Collapse>
	)
}

function CommandStringPreview({commandString}) {
	const [clipboardSocket, setClipboardSocket] = useState({})
	useEffect(() => {
		const sock = io(`ws://${host}:${socketServerPort}`)
		setClipboardSocket(sock)
	}, [])

	function copyToClipboard() {
		clipboardSocket.emit('clipboard', {'text': commandString})
	}
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='center'
			justifyContent='center' 
			spacing={0}
			direction='row'
		>
			<Typography 
				sx={{fontFamily: 'Monospace'}}
				onClick={copyToClipboard}>
				{`${commandString}`}
			</Typography>
		</Grid>
	)
}

function UseCrosshairsSwitch({useCrosshairs, setUseCrosshairs, options}) {
	const [fsleyesSocket, setFsleyesSocket] = useState({})
	useEffect(() => {
		const sock = io(`ws://${host}:${socketServerPort}`)
		setFsleyesSocket(sock)
	}, [])
	let fsleyesString = `fsleyes ${options['input']} ${options['output']} -cm red -a 80`
	return (
		<Grid 
			container 
			item 
			xs={12}
			alignItems='left'
			spacing={0}
			direction='row'
		>
			<FormControlLabel control={<Switch checked={useCrosshairs} onChange={()=>{setUseCrosshairs(!useCrosshairs)}} />} label="use crosshair position to set starting point" />
			<Tooltip title={fsleyesString}>
				<Button style={{margin:0, textTransform: 'none'}} onClick={() => {fsleyesSocket.emit('run', {'run': fsleyesString})}}>open FSLeyes</Button>
			</Tooltip>
		</Grid>

	)
}

export default function Bet() {
	const title = "BET"
  const defaultBetOpts = {
		'input': 		'input', 		// input file path
		'output': 	'output', 		// output file path
		'-o': 				false,
		'-m': 				false,
		'-s': 				false,
		'-n': 				false,
		'-f': 				'0.5',
		'-g': 				'0',
		'-r': 				null,
		'-c': 				null,
		'-t': 				false,
		'-e': 				false,
		'-R': 				null,
		'-S': 				null,
		'-B': 				null,
		'-Z': 				null,
		'-F': 				null,
		'-A': 				null,
		'-A2': 				null,
		'-v': 				false,
		'-d': 				false
	}
	const [betOpts, setBetOpts] = useState(defaultBetOpts)
	const [useCrosshairs, setUseCrosshairs] = useState(false) // unused for now since it causes too many slow rerenderings
	// the boolean variable to show or hide the snackbar
	const [showSnackBar, setShowSnackBar] = useState(false)
	// the message, and message setter for the snackbar
	const [snackBarMessage, setSnackBarMessage] = useState('')
	const [moreOptions, setMoreOptions] = useState(false)
	const [commandString, setCommandString] = useState('')
	const [isRunning, setIsRunning] = useState(false)
	const [niivueImage, setNiivueImage] = useState('')
	const [voxStr, setVoxStr] = useState('')
	const [runSocket, setRunSocket] = useState({})

	useEffect(() => {
		const sock = io(`ws://${host}:${socketServerPort}`)
		sock.on('run', (data) => {
			console.log('run', data) 
			if (data['status'] === 0) {
				let url = `http://${host}:${fileServerPort}/file/?filename=${betOpts['output']}`
				console.log(url)
				addNiiVueImage(url,'red')
			}
			setIsRunning(false)
		})
		setRunSocket(sock)
	}, [betOpts['output']])
	
	if (host === null || socketServerPort === null || fileServerPort === null){
		setSnackBarMessage('unable to contact backend application')
	}

	nv.on('location', (location)=>{
		setVoxStr(`${location.vox[0]} ${location.vox[1]} ${location.vox[2]}`)
		//updateBetOptsValue('-c', voxStr)
	})

	function handleSnackBarClose() {
		setShowSnackBar(false)
	}

	function showSnackBarIfMsg() {
		if (snackBarMessage !== '') {
			setShowSnackBar(true)
		}
	}
	// whenever the snackbar message changes, run the effect (e.g. show the snackbar to the user when the message is updated)
	useEffect(() => {showSnackBarIfMsg()}, [snackBarMessage])
	useEffect(() => {updateCommandString()}, [betOpts])
	useEffect(() => {
		if (useCrosshairs) {
			updateBetOptsValue('-c', voxStr)
		}
	}, [voxStr])
	useEffect(() => {
		if (voxStr !== '') {
			updateBetOptsValue('-c', '')
		}
	}, [useCrosshairs])

	function setOutputName(fileName) {
		if (fileName === '' || typeof fileName === 'undefined') {
			return ''
		}
		let extIdx = fileName.lastIndexOf('.nii')
		if (extIdx < 0) {
			return fileName + '_brain.nii.gz'
		} else {
			return fileName.substring(0, extIdx) + '_brain.nii.gz'
		}
	}

	function shortenFileName(name) {
		if (useShortDisplay) {
			return name.split('/').pop() //assumes mac or linux for now...
		} else {
			return name
		}
	}

	function updateCommandString() {
		let command = 'bet '
		for (let [key, value] of Object.entries({...betOpts})) {
			if (value !== null) {
				if (value === false || value === defaultBetOpts[key]){
					continue
				} else if (value === true) {
					// if true then just pass the key to set boolean bet values
					value = ''	
				}
				command += `${(key=='input' || key=='output') ? '': key} ${(key=='input' || key=='output') ? shortenFileName(value) : value} `
			}
		}
		setCommandString(command)
	}


	function handleMoreOptions() {
		setMoreOptions(!moreOptions)
	}


	function updateBetOptsValue(option, value) {
		setBetOpts(defaultBetOpts => ({
			...defaultBetOpts,
			...{[option]: value}
		}))
		if (option === 'input') {
			setBetOpts(defaultBetOpts => ({
				...defaultBetOpts,
				...{'output': setOutputName(value)}
			}))
		}
		//updateCommandString()
	}

  return (
		<Container component="main" style={{height: '100%'}}>
			<CssBaseline enableColorScheme />
			<Box sx={{
				display: 'flex',
				flexDirection: 'column',
				alignItems: 'center',
				height: '100%'
				}}>
				<Title text='BET' />
				<Grid container spacing={2}>
					<InputField
						updateBetOptsValue={updateBetOptsValue}
						text={betOpts['input']}
					/>
					<OutputField 
						updateBetOptsValue={updateBetOptsValue}
						text={betOpts['output']}
					/>
					<FvalueField 
						updateBetOptsValue={updateBetOptsValue}
						f={betOpts['-f']}
					/>
					<ActionButtons
						handleMoreOptions={handleMoreOptions}
						commandString={commandString}
						isRunning={isRunning}
						setIsRunning={setIsRunning}
						runSocket={runSocket}
					/>
					<OptionsContainer
						moreOptions={moreOptions}
						options={betOpts}
						updateBetOptsValue={updateBetOptsValue}
					/>
					<CommandStringPreview commandString={commandString}/>
					<UseCrosshairsSwitch useCrosshairs={useCrosshairs} setUseCrosshairs={setUseCrosshairs} options={betOpts} />
					<NiiVue url={niivueImage}/>
					<Snackbar
						anchorOrigin={{horizontal:'center', vertical:'bottom'}}
						open={showSnackBar}
						onClose={handleSnackBarClose}
						message={snackBarMessage}
						key='betSnackBar'
						autoHideDuration={5000}
					/>
				</Grid>
			</Box>
		</Container>
  )
}
