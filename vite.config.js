import { defineConfig, loadEnv} from 'vite'
import react from '@vitejs/plugin-react'
require('dotenv').config({path: '~/.fslgui.env'})

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
	base: './',
	root: '.',
	server: {
		open: 'index.html',
		fs: {
			// Allow serving files from one level up to the project root
			allow: ['..']
		}
	},
})
